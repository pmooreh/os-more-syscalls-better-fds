#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "refs.h"
#include "vmm.h"

class AddressSpace;
class File;
class BlockingLock;

class OpenFile {
public:
    size_t offset;
    StrongPtr<File> file;
    StrongPtr<BlockingLock> lock;

    OpenFile(StrongPtr<File> file);
    ssize_t read(void* buf, size_t nbyte);
};

class Process {
    
public:

    static constexpr int NFILES = 100;

    /* return the kernel process */
    static StrongPtr<Process> kernel();

    /* initialize the process subsystem */
    static void init();

    StrongPtr<AddressSpace> addressSpace;
    const int id;

    StrongPtr<OpenFile> files[NFILES];

    Process();
    virtual ~Process();

};

#endif
