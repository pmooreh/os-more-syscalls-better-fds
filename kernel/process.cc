#include "process.h"
#include "machine.h"
#include "locks.h"

struct ProcessInfo {
    StrongPtr<Process> kernelProcess;
    volatile int nextId;
};

static ProcessInfo *info;

void Process::init(void) {
    info = new ProcessInfo();
    info->nextId = 0; 
    info->kernelProcess = StrongPtr<Process> { new Process() };
}

StrongPtr<Process> Process::kernel(void) {
    return info->kernelProcess;
}

Process::Process() : addressSpace(new AddressSpace()), id(getThenIncrement(&info->nextId,1)) {
   
}

Process::~Process() {
    for (int i=0; i<NFILES; i++) {
        files[i].reset();
    }     
}

//////////////
// OpenFile //
//////////////

OpenFile::OpenFile(StrongPtr<File> file) : offset(0), file(file), lock(new BlockingLock()) {
}

ssize_t OpenFile::read(void* buf, size_t nbyte) {
    lock->lock();

    ssize_t n = file->read(offset,buf,nbyte);
    if (n > 0) {
        offset += n;
    }

    lock->unlock();

    return n;
}
