#include "sys.h"
#include "stdint.h"
#include "idt.h"
#include "debug.h"
#include "thread.h"
#include "kernel.h"
#include "elf.h"

extern "C" int sysHandler(uint32_t eax, uint32_t *frame) {
    MISSING();
    return -1;
}

extern "C" void sysAsmHandler();

void SYS::init(void) {
    IDT::trap(48,(uint32_t)sysAsmHandler,3);
}
