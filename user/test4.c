#include "libc.h"

int main() {
    putstr("*** hello\n");

    putstr("*** fork\n");
    int id = fork();

    if (id == 0) {
        /* child */
        putstr("*** child\n");
    } else {
        /* parent */
        join(id);
        putstr("*** parent\n");
    }

    return 0;
}
