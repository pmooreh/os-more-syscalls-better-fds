#ifndef _LIBC_H_
#define _LIBC_H_

#include "sys.h"

extern void putstr(char *p);
extern void putdec(int v);
extern void puthex(long v);

extern void saych(char* s, char v);
extern void saydec(char* s, int v);
extern void saystr(char* s);

#define SHOW(x) saydec(#x,(x))


extern ssize_t readAll(int fd, void* buf, size_t nbyte);

#endif
