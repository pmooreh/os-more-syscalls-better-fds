#include "libc.h"

static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

void putstr(char* p) {
    char c;
    while ((c = *p++) != 0) putch(c); 
}

void puthex(long v) {
    for (int i=0; i<sizeof(long)*2; i++) {
          char c = hexDigits[(v & 0xf0000000) >> 28];
          putch(c);
          v = v << 4;
    }
}

void putdec(int v) {
    if (v < 0) {
        putch('-');
        v = -v;
        if (v < 0) {
            putstr("2147483647");
            return;
        }
    }
       
    if (v >= 10) {
        putdec(v / 10);
    }
    putch(hexDigits[v % 10]);
}

void saystr(char* s) {
    putstr("*** ");
    putstr(s);
    putstr("\n");
}

void saych(char* s, char c) {
    putstr("*** ");
    putstr(s);
    putstr(" ");
    putch(c);
    putstr("\n");
}

void saydec(char* s, int v) {
    putstr("*** ");
    putstr(s);
    putstr(" ");
    putdec(v);
    putstr("\n");
}


ssize_t readAll(int fd, void* buf, size_t nbyte) {
    size_t togo = nbyte;

    while (togo > 0) {
        ssize_t n = read(fd,buf,togo);
        if (n < 0) return n;
        if (n == 0) break;
        togo -= n;
    }
    return nbyte - togo;
}
