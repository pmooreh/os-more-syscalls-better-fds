#include "libc.h"

int main() {
    saystr("hello");

    saystr("open bad file");
    int fd = open("notthere");
    SHOW(fd);

    saystr("len of bad file");
    int n = len(fd);
    SHOW(n);

    char buf[100];

    saystr("read from bad file");
    int c = read(fd,buf,1);
    SHOW(c);    
    
    saystr("survive closing nothing");
    close(fd);

    saystr("open good file");
    fd = open("hello.txt");
    SHOW(fd);

    saystr("open again");
    fd = open("hello.txt");
    SHOW(fd);

    close(0);
    close(1); 

    saystr("len of closed file");
    n = len(0);
    SHOW(n);

    saystr("read from closed file");
    n = read(0,buf,1);
    SHOW(n);

    saystr("open it again");
    fd = open("hello.txt");
    SHOW(fd);
    
    c = readAll(fd,buf,1000000);
    SHOW(c);
    buf[c] = 0;
    saystr(buf);

    return 0;
}
