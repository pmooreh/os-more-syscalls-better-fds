#include "libc.h"

int main() {
    putstr("*** hello\n");

    int sem = semaphore(0);

    putstr("*** open hello.txt\n");
    int fd = open("hello.txt");
    SHOW(fd);

    char c = '.';

    read(fd,&c,1);
    saych("before fork: ",c);

    int id = fork();

    if (id == 0) {
        /* child */
        
        saych("child after fork",c);
        read(fd,&c,1);
        saych("child read",c);
        up(sem);
    } else {
        /* parent */
        down(sem);
        saych("parent after fork",c);
        read(fd,&c,1);
        saych("parent read",c);
    }

    return 0;
}
